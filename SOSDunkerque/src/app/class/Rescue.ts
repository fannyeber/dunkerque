import { Time } from '@angular/common';

export class Rescue {
    validated: boolean;
    title: string;
    subtitle: string;
    duration: Time;
    description: string;
    dateRescue: Date;
    sources: string;

    constructor(validated: boolean, title: string, subtitle: string, duration: Time, description: string, dateRescue: Date, sources: string) {
        this.validated = validated;
        this.title = title;
        this.subtitle = subtitle;
        this.description = description;
        this.duration = duration;
        this.dateRescue = dateRescue;
        this.sources = sources;

    }
}