
export class Boat {
    validated: boolean;
    name: string;
    type: string;
    manufacturer: string;
    manufacturingDate: Date;
    length: Date;
    width: number;
    weight: number;
    endDuty: number;
    description: string;
    isRescued: boolean;
    isRescuer: boolean;

    constructor(validated: boolean, name: string, type: string, manufacturer: string,
        manufacturingDate: Date, length: Date, width: number, weight: number,
        endDuty: number, description: string, isRescued: boolean, isRescuer: boolean) {
        this.validated = validated;
        this.name = name;
        this.type = type;
        this.manufacturer = manufacturer;
        this.manufacturingDate = manufacturingDate;
        this.length = length;
        this.width = width;
        this.weight = weight;
        this.endDuty = endDuty;
        this.description = description;
        this.isRescued = isRescued;
        this.isRescuer = isRescuer;
    }
}