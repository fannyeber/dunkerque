
export class Rescued {
    validated: boolean;
    lastname: string;
    firstname: string;
    birthDay: Date;
    description: string

    constructor(validated: boolean, lastName: string, firstName: string, birthday: Date, description: string) {
        this.validated = validated;
        this.lastname = lastName;
        this.firstname = firstName;
        this.birthDay = birthday;
        this.description = description;
    }
}