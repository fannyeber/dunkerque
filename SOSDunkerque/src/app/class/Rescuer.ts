
export class Rescuer {
    validated: boolean;
    lastname: string;
    firstname: string;
    birthDay: Date;
    description: string;
    grade: string;
    maritalStatus: string;
    genealogicalData: Date;
    career: string;
    professionalLife: string;
    decoration: string;
    detail: string;

    constructor(validated: boolean, lastname: string, firstname: string,
        birthDay: Date, description: string, grade: string, maritalStatus: string,
        genealogicalData: Date, career: string, professionalLife: string,
        decoration: string, detail: string,) {
        this.validated = validated;
        this.lastname = lastname;
        this.firstname = firstname;
        this.birthDay = birthDay;
        this.description = description;
        this.grade = grade;
        this.maritalStatus = maritalStatus;
        this.genealogicalData = genealogicalData;
        this.career = career;
        this.professionalLife = professionalLife;
        this.decoration = decoration;
        this.detail = detail;
    }
}