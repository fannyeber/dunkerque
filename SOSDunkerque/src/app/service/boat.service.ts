import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BoatService {

  private BoatUrl = "https://sos-dunkerque-back.herokuapp.com/boat";
  httpOption = {
    headers: new HttpHeaders({ 'Content-type': 'application/json' }),
  };

  constructor(private http: HttpClient) { }

  getAll(onlyValidate: boolean = false): Observable<any> {
    return this.http.get<any>(`${this.BoatUrl}?validated=${onlyValidate}`, this.httpOption);
  }

  getAllRescued(onlyValidate: boolean = false): Observable<any> {
    return this.http.get(`${this.BoatUrl}/rescued?validated=${onlyValidate}`, this.httpOption);
  }

  getAllRescuer(onlyValidate: boolean = false): Observable<any> {
    return this.http.get(`${this.BoatUrl}/rescuer?validated=${onlyValidate}`, this.httpOption);
  }

  get(id: any): Observable<any> {
    return this.http.get<any>(`${this.BoatUrl}/${id}`, this.httpOption);
  }

  post(object: any): Observable<any> {
    return this.http.post<any>(`${this.BoatUrl}`, object, this.httpOption)
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${this.BoatUrl}/${id}`, this.httpOption);
  }

  patch(object: any): Observable<any> {
    return this.http.patch(`${this.BoatUrl}/validate/${object.id}`, this.httpOption);
  }


}
