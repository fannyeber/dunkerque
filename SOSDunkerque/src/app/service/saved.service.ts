import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SavedService {


  private BoatUrl = "https://sos-dunkerque-back.herokuapp.com/rescued";
  httpOption = {
    headers: new HttpHeaders({ 'Content-type': 'application/json' }),
  };

  constructor(private http: HttpClient) { }

  getAll(onlyValidate: boolean = true): Observable<any> {
    return this.http.get<any>(`${this.BoatUrl}?validated=${onlyValidate}`, this.httpOption);
  }

  get(id: any): Observable<any> {
    return this.http.get<any>(`${this.BoatUrl}/${id}`, this.httpOption);
  }

  post(object: any): Observable<any> {
    return this.http.post<any>(`${this.BoatUrl}`, object, this.httpOption)
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${this.BoatUrl}/${id}`, this.httpOption);
  }

  patch(object: any): Observable<any> {
    return this.http.patch(`${this.BoatUrl}/validate/${object.id}`, this.httpOption);
  }

}
