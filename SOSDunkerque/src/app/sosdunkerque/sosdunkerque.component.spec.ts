import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SOSDunkerqueComponent } from './sosdunkerque.component';

describe('SOSDunkerqueComponent', () => {
  let component: SOSDunkerqueComponent;
  let fixture: ComponentFixture<SOSDunkerqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SOSDunkerqueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SOSDunkerqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
