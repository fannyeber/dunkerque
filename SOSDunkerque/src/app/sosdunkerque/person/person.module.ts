import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { ClarityModule } from '@clr/angular';
import { PersonComponent } from './person.component';
import { SavedComponent } from './saved/saved.component';
import { SaverComponent } from './saver/saver.component';
import { SavedDetailsComponent } from './saved/saved-details/saved-details.component';
import { PersonDetailsComponent } from './person-details/person-details.component';
import { SaverDetailsComponent } from './saver/saver-details/saver-details.component';
import { FormsModule } from '@angular/forms';
import { SavedPersonFormComponent } from './saved/saved-person-form/saved-person-form.component';
import { SaverPersonFormComponent } from './saver/saver-person-form/saver-person-form.component';

const routes: Route[] = [
  { path: '', component: PersonComponent },
  { path: 'details/:id', component: PersonDetailsComponent }
]

@NgModule({
  declarations: [
    PersonComponent,
    PersonDetailsComponent,
    SavedComponent,
    SaverComponent,
    SavedDetailsComponent,
    SaverDetailsComponent,
    SavedPersonFormComponent,
    SaverPersonFormComponent
  ],
  imports: [
    CommonModule,
    ClarityModule,
    FormsModule,
    RouterModule.forChild(routes),
    
  ]
})
export class PersonModule { }
