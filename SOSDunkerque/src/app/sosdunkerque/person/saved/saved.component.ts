import { Component, OnInit } from '@angular/core';
import { SavedService } from 'src/app/service/saved.service';

@Component({
  selector: 'app-saved',
  templateUrl: './saved.component.html',
  styleUrls: ['./saved.component.scss']
})
export class SavedComponent implements OnInit {
  openModal = false;
  public saved: any[] = [];

  constructor(private service: SavedService) { }

  ngOnInit(): void {
    this.service.getAll(true).subscribe({
      next: (res) => {
        this.saved = res;
      }
    })
  }

  openAddModal() {
    this.openModal = true;
  }
}
