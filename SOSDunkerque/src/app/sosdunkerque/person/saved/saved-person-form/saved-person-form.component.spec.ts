import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedPersonFormComponent } from './saved-person-form.component';

describe('SavedPersonFormComponent', () => {
  let component: SavedPersonFormComponent;
  let fixture: ComponentFixture<SavedPersonFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SavedPersonFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedPersonFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
