import { Component, OnInit } from '@angular/core';
import { SnotifyService } from 'ng-snotify';
import { Rescued } from 'src/app/class/Rescued';
import { SavedService } from 'src/app/service/saved.service';

@Component({
  selector: 'app-saved-person-form',
  templateUrl: './saved-person-form.component.html',
  styleUrls: ['./saved-person-form.component.scss']
})
export class SavedPersonFormComponent implements OnInit {
  lastName: string;
  firstName: string;
  birthday: Date;
  description: string

  constructor(public service: SavedService, public ss: SnotifyService) { }

  ngOnInit(): void {
  }

  create() {
    const saved = new Rescued(false, this.lastName, this.firstName, this.birthday, this.description);
    this.service.post(saved).subscribe({
      next: () => {
        this.ss.success("Ajout réussi, en attente de validation");
      }
    })
  }
}
