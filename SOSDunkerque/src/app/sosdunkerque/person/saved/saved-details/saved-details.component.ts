import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-saved-details',
  templateUrl: './saved-details.component.html',
  styleUrls: ['./saved-details.component.scss']
})
export class SavedDetailsComponent implements OnInit {

  @Input() object : any;
  birthDate:any;
  constructor() { }

  ngOnInit(): void {
  }

}
