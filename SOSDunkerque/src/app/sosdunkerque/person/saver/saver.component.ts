import { Component, OnInit } from '@angular/core';
import { Rescuer } from 'src/app/class/Rescuer';
import { SaverService } from 'src/app/service/saver.service';

@Component({
  selector: 'app-saver',
  templateUrl: './saver.component.html',
  styleUrls: ['./saver.component.scss']
})
export class SaverComponent implements OnInit {
  openModal = false;
  public savers: Rescuer[] = [];

  constructor(private service: SaverService) { }

  ngOnInit(): void {
    this.service.getAll(true).subscribe({
      next: (res) => {
        this.savers = res;
      }
    })
  }

  openAddModal() {
    this.openModal = true;
  }
}
