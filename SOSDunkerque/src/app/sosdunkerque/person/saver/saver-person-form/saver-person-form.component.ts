import { Component, OnInit } from '@angular/core';
import { SnotifyService } from 'ng-snotify';
import { Rescuer } from 'src/app/class/Rescuer';
import { SaverService } from 'src/app/service/saver.service';

@Component({
  selector: 'app-saver-person-form',
  templateUrl: './saver-person-form.component.html',
  styleUrls: ['./saver-person-form.component.scss']
})
export class SaverPersonFormComponent implements OnInit {
  lastname: string;
  firstname: string;
  birthDay: Date;
  description: string;
  grade: string;
  maritalStatus: string;
  genealogicalData: Date;
  career: string;
  professionalLife: string;
  decoration: string;
  detail: string;

  constructor(public service: SaverService, public ss: SnotifyService) { }

  ngOnInit(): void {
  }

  create() {
    const saver = new Rescuer(false, this.lastname, this.firstname, this.birthDay, this.description, this.grade, this.maritalStatus, this.genealogicalData, this.career, this.professionalLife, this.decoration, this.detail);
    this.service.post(saver).subscribe({
      next: () => {
        this.ss.success("Ajout réussi, en attente de validation");
      }
    })
  }

}
