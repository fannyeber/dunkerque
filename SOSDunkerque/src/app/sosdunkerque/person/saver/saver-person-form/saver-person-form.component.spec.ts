import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaverPersonFormComponent } from './saver-person-form.component';

describe('SaverPersonFormComponent', () => {
  let component: SaverPersonFormComponent;
  let fixture: ComponentFixture<SaverPersonFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaverPersonFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaverPersonFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
