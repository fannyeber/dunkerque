import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-saver-details',
  templateUrl: './saver-details.component.html',
  styleUrls: ['./saver-details.component.scss']
})
export class SaverDetailsComponent implements OnInit {

  @Input() object : any;
  birthDate: any;
  constructor() { }

  ngOnInit(): void {
  }

}
