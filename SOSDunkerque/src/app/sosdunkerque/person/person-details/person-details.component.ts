import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { SavedService } from 'src/app/service/saved.service';
import { SaverService } from 'src/app/service/saver.service';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.scss']
})
export class PersonDetailsComponent implements OnInit {

  object:any;
  isSaved:boolean=false;
  objectId:any;
  constructor(private loaction:Location,private savedService: SavedService,
    private saverService:SaverService) { }

  ngOnInit(): void {
    this.objectId=window.location.href.split('/')[4];
    this.savedService.get(this.objectId).subscribe({
      next:(res:any)=>{
        if(!res){
          this.saverService.get(this.objectId).subscribe({
            next:(res2)=>{
              this.object=res2;
            }
          })
        }
        else{
        this.object=res;
        }
      }
    })
  }

  goBack(){
    this.loaction.back();
  }

  get(){
    //récup l'id 
    //faire le get pour récup l'objet
  }

}
