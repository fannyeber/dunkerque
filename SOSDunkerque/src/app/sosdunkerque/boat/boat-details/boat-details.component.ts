import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { BoatService } from 'src/app/service/boat.service';

@Component({
  selector: 'app-boat-details',
  templateUrl: './boat-details.component.html',
  styleUrls: ['./boat-details.component.scss']
})
export class BoatDetailsComponent implements OnInit {

  isSaved:boolean=false;
  object:any;
  objectId:any;
  constructor(private location: Location,private boatService:BoatService) { }

  ngOnInit(): void {
    this.objectId=window.location.href.split('/')[4];
    this.boatService.get(this.objectId).subscribe({
      next:(res)=>{
        this.object=res;
      }
    })
  }

  goBack(){
    this.location.back();
  }


}
