import { Component, OnInit } from '@angular/core';
import { SnotifyService } from 'ng-snotify';
import { Boat } from 'src/app/class/Boat';
import { BoatService } from 'src/app/service/boat.service';

@Component({
  selector: 'app-boat-saver-add-form',
  templateUrl: './boat-saver-add-form.component.html',
  styleUrls: ['./boat-saver-add-form.component.scss']
})
export class BoatSaverAddFormComponent implements OnInit {
  name: string;
  type: string;
  manufacturer: string;
  manufacturingDate: Date;
  length: Date;
  width: number;
  weight: number;
  endDuty: number;
  description: string;
  isRescued = false;
  isRescuer = !this.isRescued;
  constructor(public service: BoatService, public ss: SnotifyService) { }

  ngOnInit(): void {
  }

  create() {
    const boat = new Boat(false, this.name, this.type, this.manufacturer, this.manufacturingDate,
      this.length, this.width, this.weight, this.endDuty, this.description, this.isRescued, this.isRescuer)
    this.service.post(boat).subscribe({
      next: () => {
        this.ss.success("Bateau créer, il est en attente de validations")
      }
    });
  }
}
