import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoatSaverAddFormComponent } from './boat-saver-add-form.component';

describe('BoatSaverAddFormComponent', () => {
  let component: BoatSaverAddFormComponent;
  let fixture: ComponentFixture<BoatSaverAddFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoatSaverAddFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoatSaverAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
