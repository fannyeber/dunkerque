import { Component, Input, OnInit } from '@angular/core';
import { BoatService } from 'src/app/service/boat.service';

@Component({
  selector: 'app-boat-saver-details',
  templateUrl: './boat-saver-details.component.html',
  styleUrls: ['./boat-saver-details.component.scss']
})
export class BoatSaverDetailsComponent implements OnInit {

  objectId:any;
  @Input() object:any;
  constructor(private boatService:BoatService) { }

  ngOnInit(): void {
    this.objectId=window.location.href.split('/')[4];
    this.boatService.get(this.objectId).subscribe({
      next:(res)=>{
        this.object=res;
      }
    })
  }

}
