import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoatSaverDetailsComponent } from './boat-saver-details.component';

describe('BoatSaverDetailsComponent', () => {
  let component: BoatSaverDetailsComponent;
  let fixture: ComponentFixture<BoatSaverDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoatSaverDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoatSaverDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
