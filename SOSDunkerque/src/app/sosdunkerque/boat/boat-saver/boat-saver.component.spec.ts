import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoatSaverComponent } from './boat-saver.component';

describe('BoatSaverComponent', () => {
  let component: BoatSaverComponent;
  let fixture: ComponentFixture<BoatSaverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoatSaverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoatSaverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
