import { Component, OnInit } from '@angular/core';
import { BoatService } from 'src/app/service/boat.service';

@Component({
  selector: 'app-boat-saver',
  templateUrl: './boat-saver.component.html',
  styleUrls: ['./boat-saver.component.scss']
})
export class BoatSaverComponent implements OnInit {

  public boatSaver: any[] = [];
  public openModal = false;

  constructor(private boatService: BoatService) { }

  ngOnInit(): void {
    this.boatService.getAllRescuer(true).subscribe({
      next: (res) => {
        this.boatSaver = res;
      }
    });
  }

  openAddModal() {
    this.openModal = true;
  }

}
