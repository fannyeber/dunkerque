import { Component, OnInit } from '@angular/core';
import { BoatService } from 'src/app/service/boat.service';

@Component({
  selector: 'app-boat-saved',
  templateUrl: './boat-saved.component.html',
  styleUrls: ['./boat-saved.component.scss']
})
export class BoatSavedComponent implements OnInit {
  public boatSaved: any[] = [];

  public openModal = false;

  constructor(private boatService: BoatService) {

  }

  ngOnInit(): void {
    this.boatService.getAllRescued(true).subscribe({
      next: (res) => {
        this.boatSaved = res;
      }
    });
  }


  openAddModal() {
    this.openModal = true;
  }
}
