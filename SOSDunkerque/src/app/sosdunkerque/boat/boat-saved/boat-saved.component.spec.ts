import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoatSavedComponent } from './boat-saved.component';

describe('BoatSavedComponent', () => {
  let component: BoatSavedComponent;
  let fixture: ComponentFixture<BoatSavedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoatSavedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoatSavedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
