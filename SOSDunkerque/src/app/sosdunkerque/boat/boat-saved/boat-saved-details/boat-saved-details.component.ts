import { Component, Input, OnInit } from '@angular/core';
import { BoatService } from 'src/app/service/boat.service';

@Component({
  selector: 'app-boat-saved-details',
  templateUrl: './boat-saved-details.component.html',
  styleUrls: ['./boat-saved-details.component.scss']
})
export class BoatSavedDetailsComponent implements OnInit {

  objectId:any;
  @Input() object:any;
  constructor(private boatService:BoatService) { }

  ngOnInit(): void {
    this.objectId=window.location.href.split('/')[4];
    this.boatService.get(this.objectId).subscribe({
      next:(res)=>{
        this.object=res;
      }
    })
  }

}
