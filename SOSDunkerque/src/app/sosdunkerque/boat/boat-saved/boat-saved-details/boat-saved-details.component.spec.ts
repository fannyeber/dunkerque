import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoatSavedDetailsComponent } from './boat-saved-details.component';

describe('BoatSavedDetailsComponent', () => {
  let component: BoatSavedDetailsComponent;
  let fixture: ComponentFixture<BoatSavedDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoatSavedDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoatSavedDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
