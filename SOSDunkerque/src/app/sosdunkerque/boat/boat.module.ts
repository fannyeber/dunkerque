import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Route, RouterModule } from '@angular/router';
import { ClarityModule } from '@clr/angular';
import { BoatDetailsComponent } from './boat-details/boat-details.component';
import { BoatSavedDetailsComponent } from './boat-saved/boat-saved-details/boat-saved-details.component';
import { BoatSavedComponent } from './boat-saved/boat-saved.component';
import { BoatSaverAddFormComponent } from './boat-saver/boat-saver-add-form/boat-saver-add-form.component';
import { BoatSaverDetailsComponent } from './boat-saver/boat-saver-details/boat-saver-details.component';
import { BoatSaverComponent } from './boat-saver/boat-saver.component';
import { BoatComponent } from './boat.component';

const routes: Route[] = [
  { path: '', component: BoatComponent },
  { path: 'details/:id', component: BoatDetailsComponent }
]

@NgModule({
  declarations: [
    BoatComponent,
    BoatSaverComponent,
    BoatSavedComponent,
    BoatSavedDetailsComponent,
    BoatSaverDetailsComponent,
    BoatDetailsComponent,
    BoatSaverAddFormComponent,
  ],
  imports: [
    CommonModule,
    ClarityModule,
    FormsModule,
    RouterModule.forChild(routes),
  ]
})
export class BoatModule { }
