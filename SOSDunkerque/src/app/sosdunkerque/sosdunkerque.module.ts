import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClarityModule } from '@clr/angular';
import { VoeuxComponent } from '../voeux/voeux.component';
import { SOSDunkerqueComponent } from './sosdunkerque.component';


const routes: Routes = [
  {
    path: '', component: SOSDunkerqueComponent,
    children: [
      { path: '', component: VoeuxComponent },
      { path: 'person', loadChildren: () => import('./person/person.module').then(x => x.PersonModule) },
      { path: 'boat', loadChildren: () => import('./boat/boat.module').then(x => x.BoatModule) },
      { path: 'managing', loadChildren: () => import('./managing/managing.module').then(x => x.ManagingModule) },
    ]
  },

];

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    ClarityModule,
    RouterModule.forChild(routes)
  ]
})
export class SOSDunkerqueModule { }
