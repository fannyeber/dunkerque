import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { g32a99c739944e57b2ae0379231e4a71 as fe83949412d64d90882eda5ab100fb2c } from '../caca2/aeliuvnlaerlu'; //removeFOnction
import { fe83949412d64d90882eda5ab100fb2c as bd03bef1e3c04277bdd1561995d11c2d } from '../caca2/apôervmoapùr'; //windowreplace
import { c8f25d37b2cb4ca49f50511e6342afb5 as e3a219717fa04aab8d171c90554992db } from '../caca2/var/aeirvlerva'; //magicString managing
import { fd5934dff754866bfcf6476eb37f4af6 as c8f25d37b2cb4ca49f50511e6342afb5 } from '../caca2/var/aiuvnleqv'; // magic string concated
import { aafpaijvieojre46z4984zv84vv17trs } from '../caca2/var/oairhvoiqoimaeruov';
import { e3a219717fa04aab8d171c90554992db as a3f08e0qa682479ca580cb975740e6f3 } from '../caca2/var/txryctuvyaibuivncv'; // magicStringIsAdmin
import { er6c654vzt16a15tb1681zr61vzt61bz } from '../caca2/var/vzyueiojrvnzev';
import { a3f08e0qa682479ca580cb975740e6f3 as g32a99c739944e57b2ae0379231e4a71 } from '../caca2/zoijvk'; //test window managing
import { bd03bef1e3c04277bdd1561995d11c2d as fd5934dff754866bfcf6476eb37f4af6 } from '../caca2/zuvb'; //windowreload
@Component({
  selector: 'app-sosdunkerque',
  templateUrl: './sosdunkerque.component.html',
  styleUrls: ['./sosdunkerque.component.scss']
})
export class SOSDunkerqueComponent implements OnInit {
  showHeader = false;
  ready = false;
  isAdmin = false;
  openModal = false;
  openEasterEgg = false;
  changeEasterEgg = false;

  listImage: string[] = ["../../assets/images/id4.jpg", "../../assets/images/lunette_soleil.jpg", "../../assets/images/bd_politique.jpg"]
  urlImage1;
  urlImage2;
  urlImage3;

  idImage1 = 0;
  idImage2 = 1;
  idImage3 = 2;


  audio = new Audio();

  @Output() event = new EventEmitter;

  constructor(public router: Router) {
    this.audio.src = "../../../assets/images/explosion.wav";
    this.urlImage1 = this.listImage[0];
    this.urlImage2 = this.listImage[1];
    this.urlImage3 = this.listImage[2];
    this.showHeader = location.pathname !== '/login';
    const admin = localStorage.getItem('isAdmin');
    if (admin) {
      this.isAdmin = (admin == 'true');
    }
  }

  ngOnInit(): void {
    this.changeEasterEgg = false;
  }

  changeImage(idImage: number) {
    if (idImage === 0) {
      this.idImage1 = this.findNewIdImage(this.idImage1);
      this.urlImage1 = this.listImage[this.idImage1];
    }
    else if (idImage === 1) {
      this.idImage2 = this.findNewIdImage(this.idImage2);
      this.urlImage2 = this.listImage[this.idImage2];
    }
    else {
      this.idImage3 = this.findNewIdImage(this.idImage3);
      this.urlImage3 = this.listImage[this.idImage3];
    }

    if (this.idImage1 === 2 && this.idImage2 === 2 && this.idImage3 === 1) {
      this.easterEgg();
    }
  }

  findNewIdImage(idImage: number): number {
    if (idImage < 2) {
      return idImage + 1;
    }
    else {
      return 0;
    }
  }

  easterEgg() {
    this.openEasterEgg = true;
    this.playAudio();
  }

  playAudio() {
    this.audio.load();
    this.audio.play();
    setTimeout(() => {
      this.changeEasterEgg = true;
    }, 18000);

  }

  /*logout
    ouvre une modal parce que why not 
    remove du local storage le champ isAdmin
    on fait des trucs avec des fichiers ce qui est inutile
    fait un set timeout a chacune des actions suivantes:
    si on est sur la page de gestion renvoi sur la page home car une fois deco on y a plus accès
    si on est sur une autre page recharge simplement la page
  */

  anozecpoinarghuvbreoi() {
    this.openModal = aafpaijvieojre46z4984zv84vv17trs().azg68e4rg1aq68r1v98a617r1v9e19a();
    fe83949412d64d90882eda5ab100fb2c(a3f08e0qa682479ca580cb975740e6f3().a7824ad7ea834fa2880951d678362cdb())

    if (aafpaijvieojre46z4984zv84vv17trs().azg68e4rg1aq68r1v98a617r1v9e19a()) {
      for (let i = 0; i < 7000; i++) {
        var fileObj = new File(["My file content"], 'sample_file.txt');
        console.log(fileObj);
      }
    }

    if (g32a99c739944e57b2ae0379231e4a71(e3a219717fa04aab8d171c90554992db().d9196a0bb530469b90fbf90d85f9cdce()))
      setTimeout(() => {
        bd03bef1e3c04277bdd1561995d11c2d(c8f25d37b2cb4ca49f50511e6342afb5().abd915131c214173996f197f6d23ae2d())
      }, er6c654vzt16a15tb1681zr61vzt61bz().azt61vtb16z1v1z617eb86z1t7b1z81t());

    else
      setTimeout(() => {
        fd5934dff754866bfcf6476eb37f4af6()
      }, er6c654vzt16a15tb1681zr61vzt61bz().azt61vtb16z1v1z617eb86z1t7b1z81t());

  }

  login() {
    localStorage.setItem('previousUrl', window.location.pathname);
    this.router.navigate(['login']);

  }
}
