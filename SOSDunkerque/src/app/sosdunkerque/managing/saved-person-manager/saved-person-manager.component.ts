import { Component, OnInit } from '@angular/core';
import { SnotifyService } from 'ng-snotify';
import { SavedService } from 'src/app/service/saved.service';

@Component({
  selector: 'app-saved-person-manager',
  templateUrl: './saved-person-manager.component.html',
  styleUrls: ['./saved-person-manager.component.scss']
})
export class SavedPersonManagerComponent implements OnInit {
  public savedPersons: any[] = [];
  constructor(private service: SavedService, public ss: SnotifyService) { }

  ngOnInit(): void {
    this.load();
  }

  load() {
    this.service.getAll(false).subscribe({
      next: (res) => {
        this.savedPersons = res;
      }
    })
  }

  validate(saver: any) {
    this.service.patch(saver).subscribe({
      next: () => {
        this.ss.success("Validation effectué");
        this.load();
      }
    });
  }

  refuse(id: any) {
    this.service.delete(id).subscribe({
      next: () => {
        this.ss.success("Refus enregistrer");
        this.load();
      }
    });
  }
}
