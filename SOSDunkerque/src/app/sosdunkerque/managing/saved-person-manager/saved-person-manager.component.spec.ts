import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedPersonManagerComponent } from './saved-person-manager.component';

describe('SavedPersonManagerComponent', () => {
  let component: SavedPersonManagerComponent;
  let fixture: ComponentFixture<SavedPersonManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SavedPersonManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedPersonManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
