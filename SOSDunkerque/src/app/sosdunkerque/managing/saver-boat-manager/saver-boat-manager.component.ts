import { Component, OnInit } from '@angular/core';
import { SnotifyService } from 'ng-snotify';
import { BoatService } from 'src/app/service/boat.service';

@Component({
  selector: 'app-saver-boat-manager',
  templateUrl: './saver-boat-manager.component.html',
  styleUrls: ['./saver-boat-manager.component.scss']
})
export class SaverBoatManagerComponent implements OnInit {
  public saverBoats: any[] = [];
  constructor(private service: BoatService, public ss: SnotifyService) { }

  ngOnInit(): void {
    this.load();
  }

  load() {
    this.service.getAllRescuer(false).subscribe({
      next: (res) => {
        this.saverBoats = res;
      }
    })
  }

  validate(saver: any) {
    this.service.patch(saver).subscribe({
      next: () => {
        this.ss.success("Validation effectué")
        this.load();
      }
    });
  }

  refuse(id: any) {
    this.service.delete(id).subscribe({
      next: () => {
        this.ss.success("Refus enregistrer");
        this.load();
      }
    });
  }

}
