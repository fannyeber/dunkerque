import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaverBoatManagerComponent } from './saver-boat-manager.component';

describe('SaverBoatManagerComponent', () => {
  let component: SaverBoatManagerComponent;
  let fixture: ComponentFixture<SaverBoatManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaverBoatManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaverBoatManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
