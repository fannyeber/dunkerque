import { Component, OnInit } from '@angular/core';
import { checkIcon, ClarityIcons, timesIcon } from '@cds/core/icon';
import '@cds/core/icon/register.js';
ClarityIcons.addIcons(checkIcon);
ClarityIcons.addIcons(timesIcon);

@Component({
  selector: 'app-managing',
  templateUrl: './managing.component.html',
  styleUrls: ['./managing.component.scss']
})
export class ManagingComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }

}
