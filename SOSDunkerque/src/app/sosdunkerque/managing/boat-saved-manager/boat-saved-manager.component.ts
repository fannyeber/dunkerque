import { Component, OnInit } from '@angular/core';
import { SnotifyService } from 'ng-snotify';
import { BoatService } from 'src/app/service/boat.service';

@Component({
  selector: 'app-boat-saved-manager',
  templateUrl: './boat-saved-manager.component.html',
  styleUrls: ['./boat-saved-manager.component.scss']
})
export class BoatSavedManagerComponent implements OnInit {
  public boatSaveds: any[] = [];
  constructor(private service: BoatService, public ss: SnotifyService) { }

  ngOnInit(): void {
    this.load();
  }

  load() {
    this.service.getAllRescued(false).subscribe({
      next: (res) => {
        this.boatSaveds = res;
      }
    })
  }

  validate(saver: any) {
    this.service.patch(saver).subscribe({
      next: () => {
        this.ss.success("Validation effectué")
        this.load();
      }
    });
  }

  refuse(id: any) {
    this.service.delete(id).subscribe({
      next: () => {
        this.ss.success("Refus enregistrer");
        this.load();
      }
    });
  }

}
