import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoatSavedManagerComponent } from './boat-saved-manager.component';

describe('BoatSavedManagerComponent', () => {
  let component: BoatSavedManagerComponent;
  let fixture: ComponentFixture<BoatSavedManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoatSavedManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoatSavedManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
