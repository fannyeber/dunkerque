import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { ClarityModule } from '@clr/angular';
import { ManagingComponent } from './managing.component';
import { SavedPersonManagerComponent } from './saved-person-manager/saved-person-manager.component';
import { SaverManagerComponent } from './saver-manager/saver-manager.component';
import { BoatSavedManagerComponent } from './boat-saved-manager/boat-saved-manager.component';
import { SaverBoatManagerComponent } from './saver-boat-manager/saver-boat-manager.component';

const routes: Route[] = [
  { path: '', component: ManagingComponent }
]

@NgModule({
  declarations: [
    ManagingComponent,
    SavedPersonManagerComponent,
    SaverManagerComponent,
    BoatSavedManagerComponent,
    SaverBoatManagerComponent
  ],
  imports: [
    ClarityModule,
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ManagingModule { }
