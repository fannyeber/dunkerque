import { Component, OnInit } from '@angular/core';
import { SnotifyService } from 'ng-snotify';
import { SaverService } from 'src/app/service/saver.service';

@Component({
  selector: 'app-saver-manager',
  templateUrl: './saver-manager.component.html',
  styleUrls: ['./saver-manager.component.scss']
})
export class SaverManagerComponent implements OnInit {
  public savers: any[] = [];
  constructor(private service: SaverService, public ss: SnotifyService) { }

  ngOnInit(): void {
    this.load();
  }

  load() {
    this.service.getAll(false).subscribe({
      next: (res) => {
        this.savers = res;
      }
    })
  }

  validate(saver: any) {
    this.service.patch(saver).subscribe({
      next: () => {
        this.ss.success("Validation effectué")
        this.load();
      }
    });
  }

  refuse(id: any) {
    this.service.delete(id).subscribe({
      next: () => {
        this.ss.success("Refus enregistrer");
        this.load();
      }
    });
  }

}
