import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaverManagerComponent } from './saver-manager.component';

describe('SaverManagerComponent', () => {
  let component: SaverManagerComponent;
  let fixture: ComponentFixture<SaverManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaverManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaverManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
