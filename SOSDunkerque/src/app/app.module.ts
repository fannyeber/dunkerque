import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClarityModule } from '@clr/angular';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SOSDunkerqueComponent } from './sosdunkerque/sosdunkerque.component';
import { VoeuxComponent } from './voeux/voeux.component';
@NgModule({
  declarations: [
    AppComponent,
    SOSDunkerqueComponent,
    LoginComponent,
    VoeuxComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    SnotifyModule,
    ClarityModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  providers: [
    { provide: 'SnotifyToastConfig', useValue: ToastDefaults },
    SnotifyService,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
