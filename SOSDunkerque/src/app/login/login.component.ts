import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SnotifyService } from 'ng-snotify';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username: string = '';
  password: string = '';
  showError = false;
  constructor(public router: Router, public ss: SnotifyService) { }

  ngOnInit(): void {
  }

  log() {
    if (this.username === 'admin' && this.password === 'admin') {
      localStorage.setItem('isAdmin', 'true');
      this.router.navigate(['home'])
    }
    else {
      this.ss.error("Mauvais identifiants !");

    }
  }

  goBack() {
    const previousUrl = localStorage.getItem('previousUrl');
    console.log(previousUrl);
    if (previousUrl) {
      this.router.navigate([previousUrl])
    }
  }
}
