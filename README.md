# SOS Dunkerque

Projet pour la nuit de l'info

## L'équipe "Back to DUT"

Notre équipe était composé de 4 personnes géniales :
- Thomas Lapp
- Julien Dubocage
- Florian Metz
- Fanny Eber

Nous nous sommes tous connus via le DUT informatique de l'IUT Robert-Schuman à Illkirch-Graffenstaden.

C'est avec une joie démesurée que nous nous sommes retrouvés pour participer à notre première nuit de l'info.

## Organisation

Après une concertation sur le choix des défis et notre vision de l'application a développer,
la répartition des rôles s'est faite de manière naturelle :
- Florian et Fanny se sont occupés du front grâce à angular en utilisant la librairie de clarity design
- Julien et Thomas se sont occupés du back avec une API Node.js en express utilisant l'ORM sequelize

## Notre projet 

- [Lien vers notre application](https://sos-dunkerque-front.herokuapp.com)
- [Lien vers notre code source de back](https://gitlab.com/fannyeber/sosdunkerqueback)
- [Lien vers notre code source de front](https://gitlab.com/fannyeber/dunkerque)

## Choix des défis
 - [Meilleurs voeux 2.0 de l'entreprise TECHNOLOGY & STRATEGY](https://www.nuitdelinfo.com/inscription/defis/320)
 - [MOVAI CODE de l'entreprise CODDITY](https://www.nuitdelinfo.com/inscription/defis/294)
 - [Christmas egg de l'entreprise CAPCOD](https://www.nuitdelinfo.com/inscription/defis/312)
 - [Vers l'infini et au-delà ! de l'entreprise APSIDE TOP](https://www.nuitdelinfo.com/inscription/defis/306)

 
## Réalisation des défis
 - Meilleurs voeux 2.0 : Une carte de voeux est visible depuis la page d'acceuil (/home)
 - MOVAI CODE disponible dans le fichier : (sosdunkerque.component.ts)[https://gitlab.com/fannyeber/dunkerque/-/blob/main/SOSDunkerque/src/app/sosdunkerque/sosdunkerque.component.ts], vers les lignes 100, n'hésité pas à regarder les fichiers d'import utilisé dans cette fonction ils sont eux aussi pas mal.
 - Christmas egg : est disponible dans toute les pages avec le footer en réalisant une combinaison bien précise d'image(image numero 3 = BD, image 4 = BD et image 6 = Thomas Pesquet avec des lunettes de soleil). On a voulu transmettre les valeur d'un clic boom et d'un remake de jurassic park.
 - Vers l'infini et au-delà ! : disponible dans : la carte de voeux, toute les pages a toute les sauces.



